package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class AccountLogin extends BasicAccount implements ApiInput {

    private String password;

    public AccountLogin() {}

    protected AccountLogin(Parcel parcel) {
        super(parcel);
        password = parcel.readString();
    }

    public AccountLogin(String clientId, String clientSecret, String email, String password) {
        super(clientId, clientSecret, email);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(password);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public AccountLogin createFromParcel(Parcel in) {
            return new AccountLogin(in);
        }

        public AccountLogin[] newArray(int size) {
            return new AccountLogin[size];
        }
    };

    @Override
    public RequestBody toRequestBody() {
        return new FormBody.Builder()
                .add("client_id", getClientId())
                .add("client_secret", getClientSecret())
                .add("email", getEmail())
                .add("password", getPassword())
                .build();
    }

    @Override
    public String toQueryString() {
        return null;
    }
}
