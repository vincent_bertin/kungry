package ca.ulaval.ima.mp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import ca.ulaval.ima.mp.model.account.TokenOutput;
import ca.ulaval.ima.mp.ui.account.AccountViewModel;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantReviewsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.app_toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_list, R.id.navigation_map, R.id.navigation_account)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        Gson gson = new Gson();
        String json = getSharedPreferences("com.kungry", MODE_PRIVATE)
                .getString("token", null);
        TokenOutput savedToken = gson.fromJson(json, TokenOutput.class);
        if (savedToken != null) {
            AccountViewModel.restoreSession(savedToken);
        }

        Intent intent = new Intent(this, RestaurantReviewsActivity.class);
        // startActivity(intent);
    }
}
