package ca.ulaval.ima.mp.model;

import okhttp3.RequestBody;

public interface ApiInput {
    RequestBody toRequestBody();
    String toQueryString();
}
