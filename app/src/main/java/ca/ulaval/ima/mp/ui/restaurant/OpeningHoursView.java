package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.OpeningHour;

public class OpeningHoursView extends LinearLayout {
    View v;
    private TextView mondayText;
    private TextView tuesdayText;
    private TextView wednesdayText;
    private TextView thursdayText;
    private TextView fridayText;
    private TextView saturdayText;
    private TextView sundayText;
    private List<OpeningHour> openingHours;
    private Map<TextView, String> daysComponents;

    public OpeningHoursView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        v = inflate(context, R.layout.opening_hours_view, this);

        mondayText = v.findViewById(R.id.mondayText);
        tuesdayText = v.findViewById(R.id.tuesdayText);
        wednesdayText = v.findViewById(R.id.wednesdayText);
        thursdayText = v.findViewById(R.id.thursdayText);
        fridayText = v.findViewById(R.id.fridayText);
        saturdayText = v.findViewById(R.id.saturdayText);
        sundayText = v.findViewById(R.id.sundayText);

        daysComponents = new HashMap<>();

        daysComponents.put(mondayText, "MON");
        daysComponents.put(tuesdayText, "TUE");
        daysComponents.put(wednesdayText, "WED");
        daysComponents.put(thursdayText, "THU");
        daysComponents.put(fridayText, "FRI");
        daysComponents.put(saturdayText, "SAT");
        daysComponents.put(sundayText, "SUN");
    }

    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
        fillData();
    }

    private void fillData() {
        for (Map.Entry<TextView, String> entry : daysComponents.entrySet()) {
            OpeningHour hours = getDayHours(entry.getValue());

            if (hours != null) {
                String text = getOpeningText(hours);

                if (text != null) {
                    entry.getKey().setText(text);
                }
            }
        }
    }

    private OpeningHour getDayHours(String day) {
        for (OpeningHour item : openingHours) {
            if (item.getDay().equals(day)) {
                return item;
            }
        }
        return null;
    }

    private String getFormattedTime(String givenTime) {
        String []items = givenTime.split(":");

        if (items.length < 2) {
            return null;
        }

        return String.format("%s:%s", items[0], items[1]);
    }

    private String getOpeningText(OpeningHour openingHour) {
        String opening = getFormattedTime(openingHour.getOpeningHour());
        String closing = getFormattedTime(openingHour.getClosingHour());

        if (opening == null || closing == null) {
            return null;
        }
        return String.format("%s à %s", opening, closing);
    }
}
