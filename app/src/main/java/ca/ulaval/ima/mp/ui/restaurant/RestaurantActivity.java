package ca.ulaval.ima.mp.ui.restaurant;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.restaurant.GetRestaurantInput;
import ca.ulaval.ima.mp.model.restaurant.Restaurant;
import ca.ulaval.ima.mp.ui.account.AccountLoginActivity;
import ca.ulaval.ima.mp.ui.account.AccountViewModel;

public class RestaurantActivity extends AppCompatActivity {

    private ImageView image;
    private TextView restaurantName;
    private TextView restaurantType;
    private TextView restaurantDistance;
    private StarsView restaurantStars;
    private TextView reviewsCount;
    private Button seeMoreReviews;
    private OpeningHoursView openingHoursView;
    private RestaurantReviewListView reviewsView;
    private Button phoneButton;
    private Button websiteButton;
    private RestaurantLocationView restaurantLocationView;
    private Button loginButton;
    private Button leaveReviewButton;
    private ImageButton backButton;

    private GetRestaurantInput input;
    private Integer restaurantId;
    private String givenRestaurantDistance;
    private Restaurant restaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        getSupportActionBar().hide();

        Intent myIntent = getIntent();
        String givenRestaurantId = myIntent.getStringExtra("restaurantId");
        givenRestaurantDistance = myIntent.getStringExtra("restaurantDistance");

        if (givenRestaurantId != null) {
            restaurantId = Integer.valueOf(givenRestaurantId);
        }

        image = findViewById(R.id.restaurantImage);
        restaurantName = findViewById(R.id.restaurantName);
        restaurantType = findViewById(R.id.restaurantType);
        restaurantDistance = findViewById(R.id.restaurantDistance);
        restaurantStars = findViewById(R.id.restaurantStars);
        reviewsCount = findViewById(R.id.reviewsCount);
        reviewsView = findViewById(R.id.reviews);
        seeMoreReviews = findViewById(R.id.seeMoreReviews);
        openingHoursView = findViewById(R.id.openingHoursView);
        phoneButton = findViewById(R.id.phoneButton);
        websiteButton = findViewById(R.id.websiteButton);
        restaurantLocationView = findViewById(R.id.locationView);
        loginButton = findViewById(R.id.loginButton);
        leaveReviewButton = findViewById(R.id.leaveReviewButton);
        backButton = findViewById(R.id.backButton);

        restaurantLocationView.setSavedInstanceBundle(savedInstanceState);

        input = new GetRestaurantInput();

        input.setId(restaurantId);

        loadData();

        final Activity that = this;

        seeMoreReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(that, RestaurantReviewsActivity.class);
                myIntent.putExtra("restaurantId", String.valueOf(restaurant.getId()));
                startActivity(myIntent);
            }
        });

        leaveReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(that, SendReviewActivity.class);
                myIntent.putExtra("restaurantId", String.valueOf(restaurant.getId()));
                startActivity(myIntent);
            }
        });

        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL,
                        Uri.parse("tel:" + restaurant.getPhoneNumber()));
                startActivity(intent);
            }
        });

        websiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = restaurant.getWebsite();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void loadData() {
        Api.getInstance().getRestaurant(input, new ApiCallback.ItemCallback<Restaurant>() {
            @Override
            public void onSuccess(Restaurant item) {
                restaurant = item;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fillData();
                    }
                });
            }
        });
    }

    private void fillData() {
        final Activity that = this;

        if (AccountViewModel.isLogged()) {
            leaveReviewButton.setVisibility(View.VISIBLE);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(that, AccountLoginActivity.class);
                    startActivity(myIntent);
                }
            });
        }
        if (restaurant != null) {
            Picasso.get().load(restaurant.getImage()).centerCrop().fit().into(image);
            String input = restaurant.getPhoneNumber();
            String number = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)",
                    "($1) $2-$3");
            RestaurantLocation restaurantLocation = new RestaurantLocation(
                    restaurant.getId(),
                    restaurant.getLocation(),
                    restaurant.getName()
            );
            if (givenRestaurantDistance == null) {
                restaurantDistance.setText(restaurant.getDistance());
            } else {
                restaurantDistance.setText(givenRestaurantDistance);
            }
            restaurantLocationView.addMarker(restaurantLocation);
            restaurantLocationView.setLocation(restaurant.getLocation());
            phoneButton.setText(number);
            openingHoursView.setOpeningHours(restaurant.getOpeningHours());
            reviewsView.setReviews(restaurant.getReviews());
            reviewsView.setReviewsCountOverride(restaurant.getReviewCount());
            restaurantName.setText(restaurant.getName());
            restaurantType.setText(restaurant.getTypeDisplay());
            restaurantStars.setNote(restaurant.getReviewAverage());
            reviewsCount.setText(String.valueOf(restaurant.getReviewCount()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        restaurantLocationView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        restaurantLocationView.onStop();
    }
    @Override
    protected void onPause() {
        restaurantLocationView.onPause();

        super.onPause();
    }
    @Override
    protected void onDestroy() {
        restaurantLocationView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        restaurantLocationView.onLowMemory();
    }
}
