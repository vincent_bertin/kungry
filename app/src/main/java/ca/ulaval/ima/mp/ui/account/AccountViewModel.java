package ca.ulaval.ima.mp.ui.account;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.KungryViewModel;
import ca.ulaval.ima.mp.model.account.AccountLogin;
import ca.ulaval.ima.mp.model.account.CreateAccountCreate;
import ca.ulaval.ima.mp.model.account.RefreshTokenInput;
import ca.ulaval.ima.mp.model.account.TokenOutput;

public class AccountViewModel extends KungryViewModel {

    private static MutableLiveData<TokenOutput> token = new MutableLiveData<>();
    private Activity parent;

    public static void restoreSession(TokenOutput savedToken) {
        token.postValue(savedToken);
        Api.getInstance().setSession(savedToken);
    }

    public void setParent(Activity parent) {
        this.parent = parent;
    }

    private void saveToken(TokenOutput token) {
        AccountViewModel.token.postValue(token);
        SharedPreferences preferences = parent
                .getSharedPreferences("com.kungry", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = gson.toJson(token);
        preferences.edit().putString("token", json).apply();
    }

    public void login(AccountLogin data, final ApiCallback.ItemCallback<TokenOutput> cb) {
        api.login(data, new ApiCallback.ItemCallback<TokenOutput>() {
            @Override
            public void onSuccess(TokenOutput item) {
                saveToken(item);
                cb.onSuccess(item);
            }
        });
    }

    public void register(CreateAccountCreate data, final ApiCallback.ItemCallback<TokenOutput> cb) {
        api.postAccount(data, new ApiCallback.ItemCallback<TokenOutput>() {
            @Override
            public void onSuccess(TokenOutput item) {
                super.onSuccess(item);
                saveToken(item);
                Log.d("Kungry", "Register OK " + item.getAccessToken());
                cb.onSuccess(item);
            }
        });
    }

    public void logout() {
        Api.getInstance().clearSession();
        SharedPreferences preferences = parent
                .getSharedPreferences("com.kungry", Context.MODE_PRIVATE);
        preferences.edit().remove("token").apply();
        token.postValue(null);
    }

    public static Boolean isLogged() {
        try {
            String at = token.getValue().getAccessToken();
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }
}