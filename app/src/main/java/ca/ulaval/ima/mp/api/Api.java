package ca.ulaval.ima.mp.api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.model.ApiInput;
import ca.ulaval.ima.mp.model.account.Account;
import ca.ulaval.ima.mp.model.account.AccountLogin;
import ca.ulaval.ima.mp.model.account.CreateAccountCreate;
import ca.ulaval.ima.mp.model.account.RefreshTokenInput;
import ca.ulaval.ima.mp.model.account.TokenOutput;
import ca.ulaval.ima.mp.model.api.PaginatedInput;
import ca.ulaval.ima.mp.model.restaurant.CreateReview;
import ca.ulaval.ima.mp.model.restaurant.GetRestaurantInput;
import ca.ulaval.ima.mp.model.restaurant.PostReviewImageInput;
import ca.ulaval.ima.mp.model.restaurant.Restaurant;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;
import ca.ulaval.ima.mp.model.restaurant.Review;
import ca.ulaval.ima.mp.model.restaurant.SearchRestaurantInput;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Api {
    private String host = "https://kungry.ca/api/v1/";
    private OkHttpClient client;
    private static Api instance = null;
    private TokenOutput currentToken;

    private Api() {
        client = new OkHttpClient.Builder()
                .build();
    }

    public static Api getInstance() {
        if (instance == null) {
            instance = new Api();
        }
        return instance;
    }

    private String getBearer() {
        if (currentToken == null) {
            return "Bearer";
        }
        return "Bearer " + currentToken.getAccessToken();
    }

    private Request.Builder getDefaultReq(String url) {
        return new Request.Builder()
                .url(host + url)
                .addHeader("Authorization", getBearer());
    }

    static public String appendParams(String url, ApiInput params) {
        return url + params.toQueryString();
    }

    private void sendReq(final Request.Builder request, final Callback cb) {
        final Request req = request.build();

        client.newCall(req).enqueue(cb);
    }

    private JSONObject getResContent(Response res) throws JSONException, IOException {
        JSONObject body = new JSONObject(res.body().string());
        JSONObject error = body.getJSONObject("error");

        if (error.length() > 0) {
            throw new IOException(error.toString());
        }

        return body.getJSONObject("content");
    }

    private JSONObject getResContent(Response res, Boolean withContent) throws JSONException, IOException {
        Log.d("Kungry", res.toString());
        JSONObject body = new JSONObject(res.body().string());
        JSONObject error = body.getJSONObject("error");

        if (error.length() > 0) {
            throw new IOException(error.toString());
        }

        if (withContent) {
            return body;
        }
        return body.getJSONObject("content");
    }

    public void postAccount(CreateAccountCreate input,
                            final ApiCallback.ItemCallback<TokenOutput> cb) {
        Request.Builder req = getDefaultReq("account").post(input.toRequestBody());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                TokenOutput tokenOutput;

                try {
                    JSONObject item = getResContent(response);
                    tokenOutput = new TokenOutput(item);
                    currentToken = tokenOutput;
                    cb.onSuccess(tokenOutput);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void login(AccountLogin input,
                      final ApiCallback.ItemCallback<TokenOutput> cb) {
        Request.Builder req = getDefaultReq("account/login").post(input.toRequestBody());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                TokenOutput tokenOutput;

                try {
                    JSONObject item = getResContent(response);
                    tokenOutput = new TokenOutput(item);
                    setSession(tokenOutput);
                    cb.onSuccess(tokenOutput);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setSession(TokenOutput token) {
        currentToken = token;
    }

    public void clearSession() {
        currentToken = null;
    }

    public void getAccountMe(final ApiCallback.ItemCallback<Account> cb) {
        Request.Builder req = getDefaultReq("account/me");

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Account account;

                try {
                    JSONObject item = getResContent(response);
                    account = new Account(item);
                    cb.onSuccess(account);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void postAccountRefreshToken(RefreshTokenInput input,
                                        final ApiCallback.ItemCallback<TokenOutput> cb) {
        input.setRefreshToken(currentToken.getRefreshToken());
        Request.Builder req = getDefaultReq("account/refresh_token").post(input.toRequestBody());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                TokenOutput tokenOutput;

                try {
                    JSONObject item = getResContent(response);
                    tokenOutput = new TokenOutput(item);
                    currentToken = tokenOutput;
                    cb.onSuccess(tokenOutput);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getRestaurants(int page, int pageSize,
                               final ApiCallback.ListCallback<RestaurantLight> cb) {
        Request.Builder req = getDefaultReq("restaurant");

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                List<RestaurantLight> restaurantLightList = new ArrayList<>();

                try {
                    JSONArray arr = getResContent(response).getJSONArray("results");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject restaurantItem = arr.getJSONObject(i);
                        RestaurantLight restaurantLight = new RestaurantLight(restaurantItem);
                        restaurantLightList.add(restaurantLight);
                    }
                    cb.onSuccess(restaurantLightList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void searchRestaurant(final SearchRestaurantInput input, final ApiCallback.ListCallback<RestaurantLight> cb) {
        Request.Builder req = getDefaultReq(appendParams("restaurant/search/", input));

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                List<RestaurantLight> restaurantLightList = new ArrayList<>();

                try {
                    JSONObject content = getResContent(response);
                    input.setPage(content.getInt("next"));
                    input.setPageSize(content.getInt("count"));
                    JSONArray arr = content.getJSONArray("results");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject restaurantItem = arr.getJSONObject(i);
                        RestaurantLight restaurantLight = new RestaurantLight(restaurantItem);
                        restaurantLightList.add(restaurantLight);
                    }
                    cb.onSuccess(restaurantLightList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getRestaurant(GetRestaurantInput params,
                              final ApiCallback.ItemCallback<Restaurant> cb) {
        Request.Builder req = getDefaultReq("restaurant" + params.toQueryString());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Restaurant restaurant;

                try {
                    JSONObject item = getResContent(response);
                    restaurant = new Restaurant(item);
                    cb.onSuccess(restaurant);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getRestaurantReviews(final GetRestaurantInput params,
                                     final ApiCallback.ListCallback<Review> cb) {
        Request.Builder req = getDefaultReq("restaurant/" + params.getId() + "/reviews");

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                List<Review> reviews = new ArrayList<>();

                try {
                    JSONObject content = getResContent(response);
                    params.getPagination().next(content);
                    JSONArray arr = content.getJSONArray("results");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject reviewItem = arr.getJSONObject(i);
                        Review review = new Review(reviewItem);
                        reviews.add(review);
                    }
                    cb.onSuccess(reviews);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void postReview(CreateReview input, final ApiCallback.ItemCallback<Review> cb) {
        Request.Builder req = getDefaultReq("review").post(input.toRequestBody());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Review createdReview;

                try {
                    JSONObject item = getResContent(response);
                    createdReview = new Review(item);
                    cb.onSuccess(createdReview);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getReviewsMine(PaginatedInput params,
                               final ApiCallback.ListCallback<Review> cb) {
        Request.Builder req = getDefaultReq(appendParams("review/mine?", params));

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                List<Review> reviews = new ArrayList<>();

                try {
                    JSONArray arr = getResContent(response).getJSONArray("results");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject reviewItem = arr.getJSONObject(i);
                        Review review = new Review(reviewItem);
                        reviews.add(review);
                    }
                    cb.onSuccess(reviews);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void postReviewImage(PostReviewImageInput input,
                                final ApiCallback.ItemCallback<Review> cb) {
        Request.Builder req = getDefaultReq(appendParams("review/", input))
                .post(input.toRequestBody());

        sendReq(req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Review createdReview;

                try {
                    JSONObject item = getResContent(response);
                    createdReview = new Review(item);
                    cb.onSuccess(createdReview);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
