package ca.ulaval.ima.mp.model.restaurant;

import ca.ulaval.ima.mp.model.ApiInput;
import ca.ulaval.ima.mp.model.api.PaginatedInput;
import okhttp3.RequestBody;

public class GetRestaurantInput implements ApiInput {
    private Double latitude;
    private Double longitude;
    private Integer id;
    private PaginatedInput pagination = new PaginatedInput();

    public GetRestaurantInput() {}

    public GetRestaurantInput(double latitude, double longitude, int id) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PaginatedInput getPagination() {
        return pagination;
    }

    public void setPagination(PaginatedInput pagination) {
        this.pagination = pagination;
    }

    @Override
    public RequestBody toRequestBody() {
        return null;
    }

    @Override
    public String toQueryString() {
        String queryString = "/" + id.toString() + "?";

        if (latitude != null) {
            queryString += "&latitude=" + latitude;
        }
        if (longitude != null) {
            queryString += "&longitude=" + longitude;
        }

        return queryString;
    }
}
