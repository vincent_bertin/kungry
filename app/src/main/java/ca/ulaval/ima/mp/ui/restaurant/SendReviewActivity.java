package ca.ulaval.ima.mp.ui.restaurant;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.restaurant.CreateReview;
import ca.ulaval.ima.mp.model.restaurant.PostReviewImageInput;
import ca.ulaval.ima.mp.model.restaurant.Review;

public class SendReviewActivity extends AppCompatActivity {

    private Api api = Api.getInstance();
    private ImageButton addPhotoButton;
    private ImageView image;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int PICK_IMAGE = 2;
    private Button sendButton;
    private StarsPickerView starsPickerView;
    private EditText comment;
    private File givenImage;
    private int restaurantId = 0;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_review);

        Intent myIntent = getIntent();
        String givenRestaurantId = myIntent.getStringExtra("restaurantId");

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.app_toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        backButton = findViewById(R.id.toolbarBackButton);
        backButton.setVisibility(View.VISIBLE);

        if (givenRestaurantId != null) {
            restaurantId = Integer.valueOf(givenRestaurantId);
        }

        addPhotoButton = findViewById(R.id.button);
        sendButton = findViewById(R.id.sendButton);
        image = findViewById(R.id.image);
        starsPickerView = findViewById(R.id.stars);
        comment = findViewById(R.id.comment);

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createReview();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case PICK_IMAGE:
                    Uri selectedImage = data.getData();
                    image.setImageURI(selectedImage);
                    givenImage = new File(getPath(selectedImage));
                    break;
            }
    }

    private String getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private void createReview() {
        CreateReview review = new CreateReview();

        review.setRestaurantId(restaurantId);
        review.setStars(starsPickerView.getNote());
        review.setComment(comment.getText().toString());
        api.postReview(review, new ApiCallback.ItemCallback<Review>() {
            @Override
            public void onSuccess(Review item) {
                if (givenImage != null) {
                    PostReviewImageInput reviewImageInput = new PostReviewImageInput(givenImage,
                            item.getId());
                    api.postReviewImage(reviewImageInput, new ApiCallback.ItemCallback<Review>() {
                        @Override
                        public void onSuccess(Review item) {
                            finish();
                        }
                    });
                } else {
                    finish();
                }
            }
        });
    }

}
