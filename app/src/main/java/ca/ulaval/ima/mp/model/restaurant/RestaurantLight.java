package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ulaval.ima.mp.model.KungryModel;

public class RestaurantLight extends KungryModel {
    private Integer id;
    private String name;
    private List<Cuisine> cuisine;
    private String type;
    private Integer reviewCount;
    private Integer reviewAverage;
    private String image;
    private String distance;
    private Location location;
    private Map<String, String> types;

    public RestaurantLight(JSONObject json) throws JSONException {
        id = json.getInt("id");
        name = json.getString("name");
        cuisine = new ArrayList<>();
        JSONArray cuisineJson = json.getJSONArray("cuisine");
        for (int i = 0; i < cuisineJson.length(); i++) {
            Cuisine cuisineItem = new Cuisine(cuisineJson.getJSONObject(i));
            cuisine.add(cuisineItem);
        }
        type = json.getString("type");
        reviewCount = json.getInt("review_count");
        reviewAverage = json.getInt("review_average");
        image = json.getString("image");
        distance = json.getString("distance");
        location = new Location(json.getJSONObject("location"));
    }

    public RestaurantLight() {}

    public RestaurantLight(Parcel parcel) {
        id = parcel.readInt();
        name = parcel.readString();
        cuisine = parcel.readParcelable(Cuisine.class.getClassLoader());
        type = parcel.readString();
        reviewCount = parcel.readInt();
        reviewAverage = parcel.readInt();
        image = parcel.readString();
        distance = parcel.readString();
        location = parcel.readParcelable(Location.class.getClassLoader());
    }

    public RestaurantLight(Integer id, String name, List<Cuisine> cuisine, String type,
                           Integer reviewCount, Integer reviewAverage, String image,
                           String distance, Location location) {
        this.id = id;
        this.name = name;
        this.cuisine = cuisine;
        this.type = type;
        this.reviewCount = reviewCount;
        this.reviewAverage = reviewAverage;
        this.image = image;
        this.distance = distance;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Cuisine> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<Cuisine> cuisine) {
        this.cuisine = cuisine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public Integer getReviewAverage() {
        return reviewAverage;
    }

    public void setReviewAverage(Integer reviewAverage) {
        this.reviewAverage = reviewAverage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        Parcelable[] cuisineArray = new Parcelable[cuisine.size()];
        cuisine.toArray(cuisineArray);
        parcel.writeTypedArray(cuisineArray, 0);
        parcel.writeString(type);
        parcel.writeInt(reviewCount);
        parcel.writeInt(reviewAverage);
        parcel.writeString(image);
        parcel.writeString(distance);
        parcel.writeParcelable(location, 0);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public RestaurantLight createFromParcel(Parcel in) {
            return new RestaurantLight(in);
        }

        public RestaurantLight[] newArray(int size) {
            return new RestaurantLight[size];
        }
    };

    public String getTypeDisplay() {
        if (types == null) {
            types = new HashMap<>();
            types.put("RESTO", "Restaurant");
            types.put("BAR", "Bar");
            types.put("SNACK", "Snack/Food • Confort food");
        }

        return types.get(type);
    }
}
