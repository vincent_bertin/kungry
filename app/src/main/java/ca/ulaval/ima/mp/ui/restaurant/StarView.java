package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;

import java.util.HashMap;
import java.util.Map;

import ca.ulaval.ima.mp.R;

public class StarView extends LinearLayout {
    @StyleableRes
    int index0 = 0;

    private Boolean isBig = false;
    private ImageView star;
    private Boolean active;
    private String size = "regular";
    private Map<String, Integer> sizeDimension;
    private Map<String, Integer> activeBackgrounds;
    private Map<String, Integer> inactiveBackgrounds;

    public StarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.star_view, this);
        int[] sets = {R.attr.active};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        boolean active = typedArray.getBoolean(index0, false);
        typedArray.recycle();

        sizeDimension = new HashMap<>();
        activeBackgrounds = new HashMap<>();
        inactiveBackgrounds = new HashMap<>();

        sizeDimension.put("small", 12);
        sizeDimension.put("regular", 20);
        sizeDimension.put("big", 40);

        activeBackgrounds.put("small", R.drawable.ic_star_active_small);
        activeBackgrounds.put("regular", R.drawable.ic_star_active_20dp);
        activeBackgrounds.put("big", R.drawable.ic_star_active_big);

        inactiveBackgrounds.put("small", R.drawable.ic_star_inactive_small);
        inactiveBackgrounds.put("regular", R.drawable.ic_star_inactive_24dp);
        inactiveBackgrounds.put("big", R.drawable.ic_star_inactive_big);

        initComponents();

        setActive(active);
        setSize("regular");
    }


    private void initComponents() {
        star = (ImageView) findViewById(R.id.star);
    }

    public Boolean getActive() {
        return active;
    }

    private int getBackgroundId() {
        try {
            if (active) {
                return activeBackgrounds.get(size);
            } else {
                return inactiveBackgrounds.get(size);
            }
        } catch (NullPointerException e) {
            size = "regular";
            return getBackgroundId();
        }
    }

    private int getComputedSize() {
        return dpToPx(sizeDimension.get(size));
    }

    private void setDimensions() {
        int computedSize = getComputedSize();

        star.setMinimumWidth(computedSize);
        star.setMinimumHeight(computedSize);
    }

    private void syncBackground() {
        star.setImageResource(getBackgroundId());
        setDimensions();
    }

    private int dpToPx(int dp) {
        float density = getContext().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    public void setActive(Boolean active) {
        this.active = active;
        syncBackground();
    }

    public void setSize(String size) {
        this.size = size;
        syncBackground();
    }
}
