package ca.ulaval.ima.mp.ui.account;

public interface AccountNavigation {
    public void goToLogin();
    public void goToRegister();
    public void afterLogin();
    public void afterRegister();
    public void afterLogout();
}
