package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class StarsPickerView extends StarsView {
    public StarsPickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initComponents() {
        super.initComponents();

        for (int i = 0; i < getMax(); i++) {
            StarView star = getStarView(i);
            final int note = i + 1;
            star.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    setNote(note);
                }
            });
        }
    }
}
