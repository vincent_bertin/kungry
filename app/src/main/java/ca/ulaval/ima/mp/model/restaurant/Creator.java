package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.KungryModel;

public class Creator extends KungryModel {
    private String firstName;
    private String lastName;

    public Creator(JSONObject json) throws JSONException {
        firstName = json.getString("first_name");
        lastName = json.getString("last_name");
    }

    private Creator(Parcel parcel) {
        firstName = parcel.readString();
        lastName = parcel.readString();
    }

    public Creator() {}

    public Creator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstName);
        parcel.writeString(lastName);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ca.ulaval.ima.mp.model.restaurant.Creator createFromParcel(Parcel in) {
            return new ca.ulaval.ima.mp.model.restaurant.Creator(in);
        }

        public ca.ulaval.ima.mp.model.restaurant.Creator[] newArray(int size) {
            return new ca.ulaval.ima.mp.model.restaurant.Creator[size];
        }
    };

    public String getDisplayName() {
        return getFirstName() + " " + getLastName();
    }
}
