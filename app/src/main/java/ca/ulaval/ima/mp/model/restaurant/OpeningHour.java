package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.KungryModel;

public class OpeningHour extends KungryModel {
    private Integer id;
    private String openingHour;
    private String closingHour;
    private String day;

    private OpeningHour(Parcel parcel) {
        id = parcel.readInt();
        openingHour = parcel.readString();
        closingHour = parcel.readString();
        day = parcel.readString();
    }

    public OpeningHour(JSONObject json) throws JSONException {
        id = json.getInt("id");
        openingHour = json.getString("opening_hour");
        closingHour = json.getString("closing_hour");
        day = json.getString("day");
    }

    public OpeningHour() {}

    public OpeningHour(Integer id, String openingHour, String closingHour, String day) {
        this.id = id;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.day = day;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(String openingHour) {
        this.openingHour = openingHour;
    }

    public String getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(String closingHour) {
        this.closingHour = closingHour;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(openingHour);
        parcel.writeString(closingHour);
        parcel.writeString(day);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OpeningHour createFromParcel(Parcel in) {
            return new OpeningHour(in);
        }

        public OpeningHour[] newArray(int size) {
            return new OpeningHour[size];
        }
    };
}
