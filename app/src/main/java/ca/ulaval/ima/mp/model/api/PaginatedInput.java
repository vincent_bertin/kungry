package ca.ulaval.ima.mp.model.api;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.RequestBody;

public class PaginatedInput implements ApiInput {
    private Integer page = 1;
    private Integer pageSize = 10;
    private Integer total = 0;

    public PaginatedInput() {
    }

    public PaginatedInput(Integer page, Integer pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public void next(JSONObject json) throws JSONException {
        page = json.getInt("next");
        total = json.getInt("count");
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotal() {
        return total;
    }

    @Override
    public RequestBody toRequestBody() {
        return null;
    }

    @Override
    public String toQueryString() {
        String queryString = "?";

        if (page != null) {
            queryString += "&page=" + page;
        }
        if (pageSize != null) {
            queryString += "&page_size=" + pageSize;
        }

        return queryString;
    }
}
