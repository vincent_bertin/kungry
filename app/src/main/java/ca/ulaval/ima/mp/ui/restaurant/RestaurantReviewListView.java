package ca.ulaval.ima.mp.ui.restaurant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.Review;

public class RestaurantReviewListView extends LinearLayout {

    View v;
    private TextView reviewsCount;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Review> reviews;
    private Integer reviewsCountOverride;

    public RestaurantReviewListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        v = inflate(context, R.layout.restaurant_reviews_view, this);
        reviews = new ArrayList<>();

        reviewsCount = v.findViewById(R.id.reviewsCountText);
        recyclerView = v.findViewById(R.id.reviewsList);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        DateFormat format = android.text.format.DateFormat.getDateFormat(getContext());

        adapter = new RestaurantReviewListAdapter(reviews);
        ((RestaurantReviewListAdapter)adapter)
                .setEventListener(new RestaurantReviewListAdapter.OnEventListener() {
            @Override
            public void onClick(Review review) {
                super.onClick(review);
            }

            @Override
            public void onReviewImageClick(Review review) {
                Intent myIntent = new Intent(getContext(), ImagePreviewActivity.class);
                myIntent.putExtra("imageUrl", review.getImage());
                getContext().startActivity(myIntent);
            }
        });
        ((RestaurantReviewListAdapter)adapter).setDateFormat(format);
        recyclerView.setAdapter(adapter);
    }

    public void setReviews(List<Review> reviews) {
        this.reviews.clear();
        addReviews(reviews);
    }

    public void addReviews(List<Review> reviews) {
        this.reviews.addAll(reviews);
        adapter.notifyDataSetChanged();
        displayCount();
    }

    private void displayCount() {
        if (reviewsCountOverride == null) {
            reviewsCount.setText(String.valueOf(reviews.size()));
        } else {
            reviewsCount.setText(String.valueOf(reviewsCountOverride));
        }
    }

    public void setReviewsCountOverride(Integer reviewsCountOverride) {
        this.reviewsCountOverride = reviewsCountOverride;
    }
}
