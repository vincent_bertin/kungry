package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import ca.ulaval.ima.mp.model.KungryModel;
import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class RefreshTokenInput extends KungryModel implements ApiInput {
    private String refreshToken;
    private String clientId;
    private String clientSecret;

    public RefreshTokenInput() {
    }

    public RefreshTokenInput(String refreshToken, String clientId, String clientSecret) {
        this.refreshToken = refreshToken;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    protected RefreshTokenInput(Parcel parcel) {
        refreshToken = parcel.readString();
        clientId = parcel.readString();
        clientSecret = parcel.readString();
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public RequestBody toRequestBody() {
        return new FormBody.Builder()
                .add("client_id", getClientId())
                .add("client_secret", getClientSecret())
                .add("refresh_token", getRefreshToken())
                .build();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(refreshToken);
        parcel.writeString(clientId);
        parcel.writeString(clientSecret);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public RefreshTokenInput createFromParcel(Parcel in) {
            return new RefreshTokenInput(in);
        }

        public RefreshTokenInput[] newArray(int size) {
            return new RefreshTokenInput[size];
        }
    };

    @Override
    public String toQueryString() {
        return null;
    }
}
