package ca.ulaval.ima.mp.ui.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import ca.ulaval.ima.mp.R;

public class AccountFragment extends Fragment implements AccountNavigation {

    public static class OnAuthListener {
        public void OnLogin() {};
        public void OnRegister() {};
    }

    private AccountLoginFragment loginFragment;
    private AccountRegisterFragment registerFragment;
    private AccountMeFragment meFragment;
    private AccountViewModel accountViewModel;
    private OnAuthListener authListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        accountViewModel =
                ViewModelProviders.of(this).get(AccountViewModel.class);
        View root = inflater.inflate(R.layout.fragment_account, container, false);

        loginFragment = new AccountLoginFragment(this);
        registerFragment = new AccountRegisterFragment(this);
        meFragment = new AccountMeFragment(this);

        if (AccountViewModel.isLogged()) {
            goToAccount();
        } else {
            goToLogin();
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!AccountViewModel.isLogged()) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    public void goToAccount() {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.accountFragment, meFragment)
                .commit();
    }

    @Override
    public void goToLogin() {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.accountFragment, loginFragment)
                .commit();
    }

    @Override
    public void goToRegister() {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.accountFragment, registerFragment)
                .commit();
    }

    @Override
    public void afterLogin() {
        if (authListener != null) {
            authListener.OnLogin();
        } else {
            goToAccount();
        }
    }

    @Override
    public void afterRegister() {
        if (authListener != null) {
            authListener.OnRegister();
        } else {
            goToAccount();
        }
    }

    @Override
    public void afterLogout() {
        goToLogin();
    }

    public void setAuthListener(OnAuthListener authListener) {
        this.authListener = authListener;
    }
}