package ca.ulaval.ima.mp.ui.account;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import ca.ulaval.ima.mp.R;

public class AccountLoginActivity extends AppCompatActivity {

    private AccountFragment fragment;
    private AccountViewModel accountViewModel;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_login_activity);
        accountViewModel =
                ViewModelProviders.of(this).get(AccountViewModel.class);
        fragment = (AccountFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        backButton = findViewById(R.id.backButton);

        fragment.setAuthListener(new AccountFragment.OnAuthListener() {
            @Override
            public void OnLogin() {
                finish();
            }

            @Override
            public void OnRegister() {
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
