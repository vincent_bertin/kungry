package ca.ulaval.ima.mp.ui.restaurant;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;

public class RestaurantLightListAdapter  extends RecyclerView.Adapter<RestaurantLightListAdapter.ViewHolder> {
    public static class OnEventListener {
        public void onClick(RestaurantLight restaurant) {
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View container;
        public TextView restaurantName;
        public TextView restaurantType;
        public StarsView noteStars;
        public TextView reviewCount;
        public TextView distance;
        public ImageView imageContainer;

        public ViewHolder(View v) {
            super(v);
            container = v.findViewById(R.id.container);
            restaurantName = v.findViewById(R.id.restaurantName);
            noteStars = v.findViewById(R.id.starsView);
            restaurantType = v.findViewById(R.id.restaurantType);
            reviewCount = v.findViewById(R.id.reviewCount);
            distance = v.findViewById(R.id.restaurantDistance);
            imageContainer = v.findViewById(R.id.restaurantImage);
        }
    }

    private RestaurantLightListAdapter.OnEventListener eventListener = null;
    private List<RestaurantLight> restaurantLightList;

    public RestaurantLightListAdapter(List<RestaurantLight> restaurantLightList) {
        this.restaurantLightList = restaurantLightList;
    }

    public void setEventListener(RestaurantLightListAdapter.OnEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public void setRestaurantLightList(List<RestaurantLight> restaurantLightList) {
        this.restaurantLightList = restaurantLightList;
    }

    @NonNull
    @Override
    public RestaurantLightListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list_item, parent, false);
        RestaurantLightListAdapter.ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.restaurantName.setText(restaurantLightList.get(position).getName());
        holder.noteStars.setNote(restaurantLightList.get(position).getReviewAverage());
        holder.reviewCount.setText(String.valueOf(restaurantLightList.get(position).getReviewCount()));
        holder.restaurantType.setText(restaurantLightList.get(position).getTypeDisplay());
        holder.distance.setText(String.valueOf(restaurantLightList.get(position).getDistance()));
        Picasso.get().load(restaurantLightList.get(position)
                .getImage())
                .centerCrop().fit()
                .into(holder.imageContainer);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventListener != null) {
                    eventListener.onClick(restaurantLightList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (restaurantLightList == null) {
            return 0;
        }
        return restaurantLightList.size();
    }
}