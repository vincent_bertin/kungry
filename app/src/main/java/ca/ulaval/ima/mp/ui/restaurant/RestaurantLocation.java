package ca.ulaval.ima.mp.ui.restaurant;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class RestaurantLocation {
    private Integer id;
    private LatLng location;
    private String name;

    public RestaurantLocation(Integer id, LatLng location, String name) {
        this.id = id;
        this.location = location;
        this.name = name;
    }

    public RestaurantLocation(Integer id, ca.ulaval.ima.mp.model.restaurant.Location location,
                              String name) {
        this(id, new LatLng(location.getLatitude(), location.getLongitude()), name);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
