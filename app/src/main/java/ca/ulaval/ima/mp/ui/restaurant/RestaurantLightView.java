package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;

public class RestaurantLightView extends LinearLayout {

    private View v;
    private RestaurantLight restaurantLight;
    public View container;
    public TextView restaurantName;
    public TextView restaurantType;
    public StarsView noteStars;
    public TextView reviewCount;
    public TextView distance;
    public ImageView imageContainer;
    private OnClickListener clickListener;

    public RestaurantLightView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        v = inflate(context, R.layout.restaurant_list_item, this);
        container = v.findViewById(R.id.container);
        restaurantName = v.findViewById(R.id.restaurantName);
        noteStars = v.findViewById(R.id.starsView);
        restaurantType = v.findViewById(R.id.restaurantType);
        reviewCount = v.findViewById(R.id.reviewCount);
        distance = v.findViewById(R.id.restaurantDistance);
        imageContainer = v.findViewById(R.id.restaurantImage);

        final View that = this;

        container.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onClick(that);
                }
            }
        });
    }

    public void setOnClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setRestaurantLight(RestaurantLight restaurantLight) {
        this.restaurantLight = restaurantLight;

        fillData();
    }

    private void fillData() {
        if (restaurantLight != null) {
            restaurantName.setText(restaurantLight.getName());
            noteStars.setNote(restaurantLight.getReviewAverage());
            reviewCount.setText(String.valueOf(restaurantLight.getReviewCount()));
            restaurantType.setText(restaurantLight.getTypeDisplay());
            distance.setText(String.valueOf(restaurantLight.getDistance()));
            Picasso.get().load(restaurantLight
                    .getImage())
                    .centerCrop().fit()
                    .into(imageContainer);
        }
    }
}
