package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.Review;

public class RestaurantReviewListAdapter extends RecyclerView.Adapter<RestaurantReviewListAdapter.ViewHolder> {

    private DateFormat dateFormat;

    public static class OnEventListener {
        public void onClick(Review review) {};
        public void onReviewImageClick(Review review) {};
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View container;
        public TextView dateText;
        public StarsView noteStars;
        public TextView authorText;
        public TextView commentText;
        public ImageView imageContainer;
        public ViewHolder(View v) {
            super(v);
            container = v.findViewById(R.id.container);
            dateText = v.findViewById(R.id.date);
            noteStars = v.findViewById(R.id.note);
            authorText = v.findViewById(R.id.author);
            commentText = v.findViewById(R.id.comment);
            imageContainer = v.findViewById(R.id.image);
        }
    }

    private OnEventListener eventListener = null;
    private List<Review> reviewsList;

    public RestaurantReviewListAdapter(List<Review> reviewsList) {
        this.reviewsList = reviewsList;
    }

    public void setBrandsList(List<Review> reviewsList) {
        this.reviewsList = reviewsList;
    }

    public void setEventListener(OnEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(reviewsList.get(position).getDate());
            if (dateFormat != null) {
                holder.dateText.setText(DateFormat.getDateInstance(DateFormat.LONG).format(date));
            }
        } catch (ParseException e) {
            holder.dateText.setText(reviewsList.get(position).getDate());
        }
        holder.noteStars.setNote(reviewsList.get(position).getStars());
        holder.authorText.setText(reviewsList.get(position).getCreator().getDisplayName());
        holder.commentText.setText(reviewsList.get(position).getComment());
        if (reviewsList.get(position).getImage() == null
                || reviewsList.get(position).getImage().equals("null")) {
            holder.imageContainer.setVisibility(View.GONE);
        } else {
            holder.imageContainer.setVisibility(View.VISIBLE);
            Picasso.get().load(reviewsList.get(position).getImage()).into(holder.imageContainer);
        }
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventListener != null) {
                    eventListener.onClick(reviewsList.get(position));
                }
            }
        });
        holder.imageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventListener != null) {
                    eventListener.onReviewImageClick(reviewsList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reviewsList.size();
    }

    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }
}
