package ca.ulaval.ima.mp.ui.restaurant;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.restaurant.GetRestaurantInput;
import ca.ulaval.ima.mp.model.restaurant.Review;
import ca.ulaval.ima.mp.ui.account.AccountLoginActivity;
import ca.ulaval.ima.mp.ui.account.AccountViewModel;

public class RestaurantReviewsActivity extends AppCompatActivity {

    private Button leaveReviewButton;
    private Button signInButton;
    private GetRestaurantInput input;
    private RestaurantReviewListView reviewsView;
    private ScrollView scrollView;
    private Boolean loading = false;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.app_toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        backButton = findViewById(R.id.toolbarBackButton);
        backButton.setVisibility(View.VISIBLE);

        Intent myIntent = getIntent();
        final String givenRestaurantId = myIntent.getStringExtra("restaurantId");

        setContentView(R.layout.activity_restaurant_reviews);

        leaveReviewButton = findViewById(R.id.leaveReviewButton);
        signInButton = findViewById(R.id.signInButton);
        reviewsView = findViewById(R.id.reviews);
        scrollView = findViewById(R.id.scrollView);

        input = new GetRestaurantInput();
        input.setId(Integer.valueOf(givenRestaurantId));

        final Activity that = this;

        leaveReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(that, SendReviewActivity.class);
                myIntent.putExtra("restaurantId", givenRestaurantId);
                startActivity(myIntent);
            }
        });

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                if (diff <= 300 && !loading) {
                    loadMore();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(that, AccountLoginActivity.class);
                startActivity(myIntent);
            }
        });

        displayActionButton();
        loadMore();
    }

    private void displayActionButton() {
        if (AccountViewModel.isLogged()) {
            leaveReviewButton.setVisibility(View.VISIBLE);
            signInButton.setVisibility(View.GONE);
        } else {
            leaveReviewButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        displayActionButton();
        input.getPagination().setPage(0);
        reviewsView.setReviews(new ArrayList<Review>());
        loadMore();
    }

    private void loadMore() {
        loading = true;
        Api.getInstance().getRestaurantReviews(input, new ApiCallback.ListCallback<Review>() {
            @Override
            public void onSuccess(final List<Review> list) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reviewsView.setReviewsCountOverride(input.getPagination().getTotal());
                        reviewsView.addReviews(list);
                        loading = false;
                    }
                });
            }
        });
    }
}
