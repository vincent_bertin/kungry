package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import ca.ulaval.ima.mp.model.KungryModel;

public class TokenOutput extends KungryModel implements Serializable {
    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private String scope;
    private Integer expiresIn;

    public TokenOutput(Parcel parcel) {
        accessToken = parcel.readString();
        tokenType = parcel.readString();
        refreshToken = parcel.readString();
        scope = parcel.readString();
        expiresIn = parcel.readInt();
    }

    public TokenOutput(JSONObject json) throws JSONException {
        accessToken = json.getString("access_token");
        tokenType = json.getString("token_type");
        refreshToken = json.getString("refresh_token");
        scope = json.getString("scope");
        expiresIn = json.getInt("expires_in");
    }

    public TokenOutput(String accessToken, String tokenType, String refreshToken,
                       String scope, Integer expiresIn) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.scope = scope;
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(accessToken);
        parcel.writeString(tokenType);
        parcel.writeString(refreshToken);
        parcel.writeString(scope);
        parcel.writeInt(expiresIn);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public TokenOutput createFromParcel(Parcel in) {
            return new TokenOutput(in);
        }

        public TokenOutput[] newArray(int size) {
            return new TokenOutput[size];
        }
    };
}
