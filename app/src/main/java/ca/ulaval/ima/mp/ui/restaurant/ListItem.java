package ca.ulaval.ima.mp.ui.restaurant;

public interface ListItem {
    public Integer getId();
    public String getLabel();
}
