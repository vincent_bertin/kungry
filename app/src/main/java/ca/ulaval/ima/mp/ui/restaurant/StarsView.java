package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;

import ca.ulaval.ima.mp.R;

public class StarsView extends LinearLayout {
    @StyleableRes
    int index0 = 0;
    @StyleableRes
    int index1 = 1;

    private int note;
    private StarView[] stars;
    private String size = "regular";

    public StarsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.stars_view, this);
        stars = new StarView[5];
        int[] sets = {R.attr.note, R.attr.size};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        int note = typedArray.getInt(index0, 0);
        String size = typedArray.getString(index1);
        typedArray.recycle();

        initComponents();

        setNote(note);
        setSize(size);
    }

    protected void initComponents() {
        stars[0] = findViewById(R.id.star1);
        stars[1] = findViewById(R.id.star2);
        stars[2] = findViewById(R.id.star3);
        stars[3] = findViewById(R.id.star4);
        stars[4] = findViewById(R.id.star5);
    }

    protected StarView getStarView(int index) {
        return stars[index];
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
        syncNote();
    }

    private void syncNote() {
        for (int i = 1; i <= stars.length; i++) {
            stars[i - 1].setActive(note >= i);
        }
    }

    public int getMax() {
        return stars.length;
    }

    public void setSize(String size) {
        this.size = size;
        for (int i = 1; i <= stars.length; i++) {
            stars[i - 1].setSize(size);
        }
    }
}
