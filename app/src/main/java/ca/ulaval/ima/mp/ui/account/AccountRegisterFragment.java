package ca.ulaval.ima.mp.ui.account;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.account.CreateAccountCreate;
import ca.ulaval.ima.mp.model.account.TokenOutput;

public class AccountRegisterFragment extends Fragment {

    private Api api = Api.getInstance();
    private View v;
    private AccountNavigation parentNav;
    private EditText firstnameInput;
    private EditText lastnameInput;
    private EditText emailInput;
    private EditText passwordInput;
    private Button button;
    private TextView goToSignInButton;
    private AccountViewModel accountViewModel;

    public AccountRegisterFragment() {
    }

    public AccountRegisterFragment(AccountNavigation parentNav) {
        this.parentNav = parentNav;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_account_register, container, false);

        accountViewModel =
                ViewModelProviders.of(this).get(AccountViewModel.class);
        accountViewModel.setParent(getActivity());
        firstnameInput = v.findViewById(R.id.firstnameInput);
        lastnameInput = v.findViewById(R.id.lastnameInput);
        emailInput = v.findViewById(R.id.emailInput);
        passwordInput = v.findViewById(R.id.passwordInput);
        button = v.findViewById(R.id.signUpButton);
        goToSignInButton = v.findViewById(R.id.goToSignIn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

        goToSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (parentNav != null) {
                    Log.d("Kungry", "not null");
                    parentNav.goToLogin();
                } else {
                    Log.d("Kungry", "null");
                }
            }
        });

        return v;
    }

    private void register() {
        CreateAccountCreate data = new CreateAccountCreate(
                getString(R.string.client_id),
                getString(R.string.client_secret),
                emailInput.getText().toString(),
                passwordInput.getText().toString(),
                firstnameInput.getText().toString(),
                lastnameInput.getText().toString()
        );

        accountViewModel.register(data, new ApiCallback.ItemCallback<TokenOutput>() {
            @Override
            public void onSuccess(TokenOutput item) {
                parentNav.afterRegister();
            }
        });
    }
}
