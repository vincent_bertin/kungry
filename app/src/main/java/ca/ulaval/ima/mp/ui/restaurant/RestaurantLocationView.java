package ca.ulaval.ima.mp.ui.restaurant;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.Location;

public class RestaurantLocationView extends LinearLayout
        implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    @StyleableRes
    int index0 = 0;

    private Boolean isFullscreen;
    private View v;
    private MapView mapView;
    private GoogleMap map;
    private Location location;
    private Bundle savedInstanceBundle;
    private List<RestaurantLocation> markers = new ArrayList<>();
    private MarkerOptions marker = new MarkerOptions();
    private LatLng mapCenter;
    private GoogleMap.OnCameraMoveListener moveListener;
    private GoogleMap.OnMarkerClickListener markerClickListener;

    public RestaurantLocationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        v = inflate(context, R.layout.restaurant_location_view, this);

        int[] sets = {R.attr.fullscreen};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        isFullscreen = typedArray.getBoolean(index0, false);

        mapView = findViewById(R.id.mapView);

        if (isFullscreen) {
            ConstraintLayout titleLayout = v.findViewById(R.id.titleContainer);

            titleLayout.setVisibility(GONE);
            LayoutParams params = new LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT
            );
            params.setMargins(0, 0, 0, 0);
            mapView.setLayoutParams(params);
        }
    }

    public void setSavedInstanceBundle(Bundle savedInstanceBundle) {
        this.savedInstanceBundle = savedInstanceBundle;
        initMap();
    }

    private void initMap() {
        mapView.onCreate(savedInstanceBundle);
        mapView.getMapAsync(this);
    }

    public void addMarker(RestaurantLocation location) {
        markers.add(location);
    }

    public void setMoveListener(GoogleMap.OnCameraMoveListener moveListener) {
        this.moveListener = moveListener;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mapCenter = map.getCameraPosition().target;
                if (moveListener != null) {
                    moveListener.onCameraMove();
                }
            }
        });
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMyLocationEnabled(false);
        refreshMap();
    }

    public LatLng getMapCenter() {
        return mapCenter;
    }

    private void refreshMap() {
        MapsInitializer.initialize(getContext());
        if (location != null) {
            map.setMinZoomPreference(12);
            for (RestaurantLocation restaurantMarker : markers) {
                MarkerOptions mapMarker = new MarkerOptions()
                        .position(restaurantMarker.getLocation())
                        .title(restaurantMarker.getName());
                map.addMarker(mapMarker).setTag(restaurantMarker.getId());
            }
            map.moveCamera(CameraUpdateFactory.newLatLng(
                    new LatLng(location.getLatitude(), location.getLongitude())
            ));
        }
    }

    public void setLocation(Location location) {
        this.location = location;
        if (map != null) {
            refreshMap();
        }
    }

    public void setLocation(android.location.Location location) {
        setLocation(new Location(location));
    }

    public void onStart() {
        mapView.onStart();
    }

    public void onLowMemory() {
        mapView.onLowMemory();
    }

    public void onStop() {
        mapView.onStop();
    }

    public void onPause() {
        mapView.onPause();
    }

    public void onDestroy() {
        mapView.onDestroy();
    }

    public void setMarkerClickListener(GoogleMap.OnMarkerClickListener markerClickListener) {
        this.markerClickListener = markerClickListener;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (markerClickListener != null) {
            return markerClickListener.onMarkerClick(marker);
        }
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        onMarkerClick(null);
    }
}
