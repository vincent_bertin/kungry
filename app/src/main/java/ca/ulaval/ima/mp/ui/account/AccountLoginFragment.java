package ca.ulaval.ima.mp.ui.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.account.AccountLogin;
import ca.ulaval.ima.mp.model.account.TokenOutput;

public class AccountLoginFragment extends Fragment {

    View v;
    private AccountNavigation parentNav;
    private EditText emailInput;
    private EditText passwordInput;
    private TextView goToSignUpButton;
    private Button button;
    private AccountViewModel accountViewModel;

    public AccountLoginFragment() {
    }

    public AccountLoginFragment(AccountNavigation parentNav) {
        this.parentNav = parentNav;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_account_login, container, false);
        accountViewModel =
                ViewModelProviders.of(this).get(AccountViewModel.class);

        accountViewModel.setParent(getActivity());

        emailInput = v.findViewById(R.id.emailInput);
        passwordInput = v.findViewById(R.id.passwordInput);
        goToSignUpButton = v.findViewById(R.id.goToSignUp);
        button = v.findViewById(R.id.signInButton);

        goToSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentNav.goToRegister();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccountLogin data = new AccountLogin(
                        getString(R.string.client_id),
                        getString(R.string.client_secret),
                        emailInput.getText().toString(),
                        passwordInput.getText().toString()
                );

                accountViewModel.login(data, new ApiCallback.ItemCallback<TokenOutput>() {
                    @Override
                    public void onSuccess(TokenOutput item) {
                        parentNav.afterLogin();
                    }
                });
            }
        });

        return v;
    }
}
