package ca.ulaval.ima.mp.model.restaurant;

import android.util.Log;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.RequestBody;

public class SearchRestaurantInput implements ApiInput {
    private Integer page;
    private Integer pageSize;
    private Double latitude;
    private Double longitude;
    private Integer radius;
    private String text;
    public Integer total = 0;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitutde() {
        return longitude;
    }

    public void setLongitutde(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public RequestBody toRequestBody() {
        return null;
    }

    @Override
    public String toQueryString() {
        String queryString = "?";

        if (page != null) {
            queryString = String.format("%s&page=%s", queryString, page);
        }
        if (pageSize != null) {
            queryString = String.format("%s&page_size=%s", queryString, pageSize);
        }
        if (latitude != null) {
            queryString = String.format("%s&latitude=%s", queryString, latitude);
        }
        if (longitude != null) {
            queryString = String.format("%s&longitude=%s", queryString, longitude);
        }
        if (radius != null) {
            queryString = String.format("%s&radius=%s", queryString, radius);
        }
        if (text != null) {
            queryString = String.format("%s&text=%s", queryString, text);
        }
        Log.d("Kungry", queryString);
        return queryString;
    }
}
