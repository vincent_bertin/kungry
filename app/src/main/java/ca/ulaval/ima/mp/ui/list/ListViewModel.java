package ca.ulaval.ima.mp.ui.list;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.KungryViewModel;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;
import ca.ulaval.ima.mp.model.restaurant.SearchRestaurantInput;


public class ListViewModel extends KungryViewModel {

    private MutableLiveData<List<RestaurantLight>> restaurantLights;
    private SearchRestaurantInput input;

    public ListViewModel() {
    }

    public MutableLiveData<List<RestaurantLight>> getRestaurantLights() {
        if (restaurantLights == null) {
            restaurantLights = new MutableLiveData<List<RestaurantLight>>();
            restaurantLights.postValue(new ArrayList<RestaurantLight>());
            loadRestaurantsLight();
        }
        return restaurantLights;
    }

    public void loadRestaurantsLight() {
        final ListViewModel that = this;

        api.searchRestaurant(input, new ApiCallback.ListCallback<RestaurantLight>() {
            @Override
            public void onSuccess(List<RestaurantLight> list) {
                List<RestaurantLight> old = restaurantLights.getValue();

                old.addAll(list);
                that.restaurantLights.postValue(list);
            }
        });
    }

    public void setInput(SearchRestaurantInput input) {
        this.input = input;
    }
}