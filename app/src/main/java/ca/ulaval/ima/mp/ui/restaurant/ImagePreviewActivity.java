package ca.ulaval.ima.mp.ui.restaurant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ca.ulaval.ima.mp.R;

public class ImagePreviewActivity extends AppCompatActivity {

    private ImageView imagePreviewView;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        getSupportActionBar().hide();

        Intent myIntent = getIntent();
        String givenImageUrl = myIntent.getStringExtra("imageUrl");

        imagePreviewView = findViewById(R.id.imagePreviewView);

        Picasso.get().load(givenImageUrl).into(imagePreviewView);
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
