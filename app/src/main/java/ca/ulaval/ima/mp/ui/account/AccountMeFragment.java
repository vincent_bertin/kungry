package ca.ulaval.ima.mp.ui.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.account.Account;

public class AccountMeFragment extends Fragment {

    private AccountNavigation parentNav;
    private TextView accountNameText;
    private TextView accountEmailText;
    private TextView accountReviewsCountText;
    private Button logoutButton;
    private View v;
    private AccountViewModel accountViewModel;

    public AccountMeFragment() {
    }

    public AccountMeFragment(AccountNavigation parentNav) {
        this.parentNav = parentNav;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_account_me, container, false);
        accountViewModel =
                ViewModelProviders.of(this).get(AccountViewModel.class);

        accountViewModel.setParent(getActivity());

        accountNameText = v.findViewById(R.id.accountNameText);
        accountEmailText = v.findViewById(R.id.accountEmailText);
        accountReviewsCountText = v.findViewById(R.id.accountReviewsCountText);
        logoutButton = v.findViewById(R.id.logoutButton);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountViewModel.logout();
                parentNav.afterLogout();
            }
        });

        Api.getInstance().getAccountMe(new ApiCallback.ItemCallback<Account>() {
            @Override
            public void onSuccess(final Account item) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            accountNameText.setText(item.getDisplayName());
                            accountEmailText.setText(item.getEmail());
                            accountReviewsCountText.setText(String.valueOf(item.getTotalReviewCount()));
                        }
                    });
                }
            }
        });
        return v;
    }
}
