package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ulaval.ima.mp.model.KungryModel;

public class Restaurant extends KungryModel {
    private Integer id;
    private List<Cuisine> cuisine;
    private String distance;
    private Integer reviewCount;
    private List<OpeningHour> openingHours;
    private Integer reviewAverage;
    private Location location;
    private List<Review> reviews;
    private String name;
    private String website;
    private String phoneNumber;
    private String image;
    private String type;
    private Map<String, String> types;

    public Restaurant(JSONObject json) throws JSONException {
        id = json.getInt("id");

        cuisine = new ArrayList<>();
        JSONArray cuisineJson = json.getJSONArray("cuisine");
        for (int i = 0; i < cuisineJson.length(); i++) {
            Cuisine cuisineItem = new Cuisine(cuisineJson.getJSONObject(i));
            cuisine.add(cuisineItem);
        }

        distance = json.getString("distance");
        reviewCount = json.getInt("review_count");

        openingHours = new ArrayList<>();
        JSONArray openingHoursJson = json.getJSONArray("opening_hours");
        for (int i = 0; i < openingHoursJson.length(); i++) {
            OpeningHour openingHourItem = new OpeningHour(openingHoursJson.getJSONObject(i));
            openingHours.add(openingHourItem);
        }

        reviewAverage = json.getInt("review_average");
        location = new Location(json.getJSONObject("location"));

        reviews = new ArrayList<>();
        JSONArray reviewsJson = json.getJSONArray("reviews");
        for (int i = 0; i < reviewsJson.length(); i++) {
            Review reviewItem = new Review(reviewsJson.getJSONObject(i));
            reviews.add(reviewItem);
        }

        name = json.getString("name");
        website = json.getString("website");
        phoneNumber = json.getString("phone_number");
        image = json.getString("image");
        type = json.getString("type");
    }

    private Restaurant(Parcel parcel) {
        id = parcel.readInt();
        cuisine = parcel.createTypedArrayList(Cuisine.CREATOR);
        distance = parcel.readString();
        reviewCount = parcel.readInt();
        openingHours = parcel.createTypedArrayList(OpeningHour.CREATOR);
        reviewAverage = parcel.readInt();
        location = parcel.readParcelable(Location.class.getClassLoader());
        reviews = parcel.createTypedArrayList(Review.CREATOR);
        name = parcel.readString();
        website = parcel.readString();
        phoneNumber = parcel.readString();
        image = parcel.readString();
        type = parcel.readString();
    }

    public Restaurant() {}

    public Restaurant(Integer id, List<Cuisine> cuisine, String distance, Integer reviewCount,
                      List<OpeningHour> openingHours, Integer reviewAverage, Location location,
                      List<Review> reviews, String name, String website, String phoneNumber,
                      String image, String type) {
        this.id = id;
        this.cuisine = cuisine;
        this.distance = distance;
        this.reviewCount = reviewCount;
        this.openingHours = openingHours;
        this.reviewAverage = reviewAverage;
        this.location = location;
        this.reviews = reviews;
        this.name = name;
        this.website = website;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Cuisine> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<Cuisine> cuisine) {
        this.cuisine = cuisine;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public List<OpeningHour> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
    }

    public Integer getReviewAverage() {
        return reviewAverage;
    }

    public void setReviewAverage(Integer reviewAverage) {
        this.reviewAverage = reviewAverage;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        Parcelable[] cuisineArray = new Parcelable[cuisine.size()];
        cuisine.toArray(cuisineArray);
        parcel.writeTypedArray(cuisineArray, 0);
        parcel.writeString(distance);
        parcel.writeInt(reviewCount);
        Parcelable[] openingHoursArray = new Parcelable[openingHours.size()];
        openingHours.toArray(openingHoursArray);
        parcel.writeTypedArray(openingHoursArray, 0);
        parcel.writeInt(reviewAverage);
        parcel.writeParcelable(location, 0);
        Parcelable[] reviewsArray = new Parcelable[reviews.size()];
        reviews.toArray(reviewsArray);
        parcel.writeTypedArray(reviewsArray, 0);
        parcel.writeString(name);
        parcel.writeString(website);
        parcel.writeString(phoneNumber);
        parcel.writeString(image);
        parcel.writeString(type);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public String getTypeDisplay() {
        if (types == null) {
            types = new HashMap<>();
            types.put("RESTO", "Restaurant");
            types.put("BAR", "Bar");
            types.put("SNACK", "Snack/Food • Confort food");
        }

        return types.get(type);
    }
}
