package ca.ulaval.ima.mp.ui.map;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.restaurant.GetRestaurantInput;
import ca.ulaval.ima.mp.model.restaurant.Restaurant;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;
import ca.ulaval.ima.mp.model.restaurant.SearchRestaurantInput;
import ca.ulaval.ima.mp.ui.list.ListViewModel;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantActivity;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantLightView;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantLocation;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantLocationView;

public class MapFragment extends Fragment
        implements LocationListener, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private MapViewModel mapViewModel;
    private ListViewModel listViewModel;
    private RestaurantLocationView restaurantLocationView;
    private CardView restaurantPreviewContainer;
    private CardView helperContainer;
    private RestaurantLightView restaurantPreview;
    private Location currentLocation;
    private List<RestaurantLight> restaurantLights;
    private SearchRestaurantInput searchInput;
    private RestaurantLight selectedRestaurant;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mapViewModel =
                ViewModelProviders.of(this).get(MapViewModel.class);
        listViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_map, container, false);

        restaurantLocationView = root.findViewById(R.id.restaurantLocationView);
        restaurantPreviewContainer = root.findViewById(R.id.restaurantPreviewContainer);
        restaurantPreview = root.findViewById(R.id.restaurantPreview);
        helperContainer = root.findViewById(R.id.helperContainer);

        restaurantLocationView.setSavedInstanceBundle(savedInstanceState);

        restaurantLocationView.setMarkerClickListener(this);
        restaurantLocationView.setMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                LatLng mapCenter = restaurantLocationView.getMapCenter();
                setCurrentLocation(mapCenter);
            }
        });

        searchInput = new SearchRestaurantInput();

        searchInput.setPage(null);
        searchInput.setPageSize(null);
        searchInput.setRadius(50);
        listViewModel.setInput(searchInput);

        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, this
            );
        } catch (SecurityException e) {
            Log.d("Kungry", "Accès à la localisation rejeté");
        }

        restaurantLights = new ArrayList<>();

        listViewModel.getRestaurantLights().observe(getViewLifecycleOwner(),
                new Observer<List<RestaurantLight>>() {
                    @Override
                    public void onChanged(List<RestaurantLight> data) {
                        restaurantLights.addAll(data);
                        for (RestaurantLight restaurantLight : data) {
                            RestaurantLocation restaurantLocation = new RestaurantLocation(
                                    restaurantLight.getId(),
                                    restaurantLight.getLocation(),
                                    restaurantLight.getName()
                            );

                            restaurantLocationView.addMarker(restaurantLocation);
                        }
                        if (currentLocation != null) {
                            restaurantLocationView.setLocation(currentLocation);
                        }
                    }
                }
        );

        restaurantPreview.setOnClickListener(this);

        return root;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (currentLocation == null || (
                currentLocation.getLongitude() != location.getLongitude() ||
                        currentLocation.getLatitude() != location.getLatitude()
        )) {
            setCurrentLocation(location);
        }
    }

    private void setCurrentLocation(Location location) {
        searchInput.setLatitude(location.getLatitude());
        searchInput.setLongitutde(location.getLongitude());
        listViewModel.loadRestaurantsLight();
        currentLocation = location;
        restaurantLocationView.setLocation(currentLocation);
    }

    private void setCurrentLocation(LatLng location) {
        searchInput.setLatitude(location.latitude);
        searchInput.setLongitutde(location.longitude);
        listViewModel.loadRestaurantsLight();
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        restaurantLocationView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        restaurantLocationView.onStop();
    }
    @Override
    public void onPause() {
        restaurantLocationView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        restaurantLocationView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        restaurantLocationView.onLowMemory();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker == null) {
            restaurantPreviewContainer.setVisibility(View.GONE);
            selectedRestaurant = null;
            helperContainer.setVisibility(View.VISIBLE);
        } else {
            helperContainer.setVisibility(View.GONE);
            restaurantPreviewContainer.setVisibility(View.VISIBLE);
            restaurantPreview.setOnClickListener(this);
            Integer restaurantId = (Integer) marker.getTag();

            for (RestaurantLight restaurantLight : restaurantLights) {
                if (restaurantLight.getId().equals(restaurantId)) {
                    selectedRestaurant = restaurantLight;
                    restaurantPreview.setRestaurantLight(restaurantLight);
                }
            }
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        Intent myIntent = new Intent(getActivity(), RestaurantActivity.class);
        myIntent.putExtra("restaurantId", String.valueOf(selectedRestaurant.getId()));
        myIntent.putExtra("restaurantDistance",
                String.valueOf(selectedRestaurant.getDistance()));
        startActivity(myIntent);
    }
}