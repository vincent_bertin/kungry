package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.KungryModel;

public class Account extends KungryModel {
    private Integer totalReviewCount;
    private String lastName;
    private String firstName;
    private String email;
    private String created;
    private String updated;
    private Integer user;

    public Account() {}

    public Account(JSONObject json) throws JSONException {
        totalReviewCount = json.getInt("total_review_count");
        lastName = json.getString("last_name");
        firstName = json.getString("first_name");
        email = json.getString("email");
        created = json.getString("created");
        updated = json.getString("updated");
        user = json.getInt("user");
    }

    public Account(Integer totalReviewCount, String lastName, String firstName, String email, String created, String updated,
                   Integer user) {
        this.totalReviewCount = totalReviewCount;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.created = created;
        this.updated = updated;
        this.user = user;
    }

    protected Account(Parcel parcel) {
        totalReviewCount = parcel.readInt();
        lastName = parcel.readString();
        firstName = parcel.readString();
        email = parcel.readString();
        created = parcel.readString();
        updated = parcel.readString();
        user = parcel.readInt();
    }

    public Integer getTotalReviewCount() {
        return totalReviewCount;
    }

    public void setTotalReviewCount(Integer totalReviewCount) {
        this.totalReviewCount = totalReviewCount;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getDisplayName() {
        return getFirstName() + " " + getLastName();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(totalReviewCount);
        parcel.writeString(lastName);
        parcel.writeString(firstName);
        parcel.writeString(email);
        parcel.writeString(created);
        parcel.writeString(updated);
        parcel.writeInt(user);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };
}
