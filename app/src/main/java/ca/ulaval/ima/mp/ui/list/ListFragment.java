package ca.ulaval.ima.mp.ui.list;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;
import ca.ulaval.ima.mp.model.restaurant.SearchRestaurantInput;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantActivity;
import ca.ulaval.ima.mp.ui.restaurant.RestaurantLightListAdapter;

public class ListFragment extends Fragment implements LocationListener {

    private ListViewModel listViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<RestaurantLight> restaurantLights;
    private SearchRestaurantInput searchInput;
    private ScrollView scrollView;
    private Location currentLocation = null;
    private Boolean loading = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        listViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list, container, false);

        searchInput = new SearchRestaurantInput();

        searchInput.setPage(1);
        searchInput.setPageSize(10);
        searchInput.setRadius(50);
        listViewModel.setInput(searchInput);

        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (SecurityException e) {
            Log.d("Kungry", "Accès à la localisation rejeté");
        }

        listViewModel.getRestaurantLights().observe(getViewLifecycleOwner(),
            new Observer<List<RestaurantLight>>() {
                @Override
                public void onChanged(List<RestaurantLight> data) {
                    restaurantLights.addAll(data);
                    adapter.notifyDataSetChanged();
                    loading = false;
                }
            }
        );

        restaurantLights = new ArrayList<>();
        recyclerView = root.findViewById(R.id.restaurantsLightList);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RestaurantLightListAdapter(restaurantLights);
        recyclerView.setAdapter(adapter);

        scrollView = root.findViewById(R.id.scrollView);

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                if (diff <= 300 && !loading) {
                    loadMore();
                }
            }
        });

        ((RestaurantLightListAdapter)adapter)
            .setEventListener(new RestaurantLightListAdapter.OnEventListener() {
                @Override
                public void onClick(RestaurantLight restaurant) {
                    Intent myIntent = new Intent(getActivity(), RestaurantActivity.class);
                    myIntent.putExtra("restaurantId", String.valueOf(restaurant.getId()));
                    myIntent.putExtra("restaurantDistance",
                            String.valueOf(restaurant.getDistance()));
                    startActivity(myIntent);
                }
        });

        return root;
    }

    private void loadMore() {
        loading = true;
        listViewModel.loadRestaurantsLight();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (currentLocation == null || (
                currentLocation.getLongitude() != location.getLongitude() ||
                currentLocation.getLatitude() != location.getLatitude()
            )) {
            searchInput.setLatitude(location.getLatitude());
            searchInput.setLongitutde(location.getLongitude());
            listViewModel.loadRestaurantsLight();
            currentLocation = location;
        }
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }
}