package ca.ulaval.ima.mp;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.List;

import ca.ulaval.ima.mp.api.Api;
import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.ApiInput;
import ca.ulaval.ima.mp.model.account.Account;
import ca.ulaval.ima.mp.model.account.CreateAccountCreate;
import ca.ulaval.ima.mp.model.account.RefreshTokenInput;
import ca.ulaval.ima.mp.model.account.TokenOutput;
import ca.ulaval.ima.mp.model.api.PaginatedInput;
import ca.ulaval.ima.mp.model.restaurant.GetRestaurantInput;
import ca.ulaval.ima.mp.model.restaurant.Restaurant;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;
import ca.ulaval.ima.mp.model.restaurant.Review;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_list, R.id.navigation_map, R.id.navigation_account)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }
}
