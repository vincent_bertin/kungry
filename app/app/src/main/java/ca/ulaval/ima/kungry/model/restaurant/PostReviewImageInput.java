package ca.ulaval.ima.mp.model.restaurant;

import java.io.File;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PostReviewImageInput implements ApiInput {
    private File image;
    private Integer id;

    public PostReviewImageInput(File image, Integer id) {
        this.image = image;
        this.id = id;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public RequestBody toRequestBody() {
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", "image",
                        RequestBody.create(MediaType.parse("application/octet-stream"), image))
                .build();
    }

    @Override
    public String toQueryString() {
        return "id=" + id.toString();
    }
}
