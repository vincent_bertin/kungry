package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.KungryModel;

public class Cuisine extends KungryModel {
    private Integer id;
    private String name;

    protected Cuisine(Parcel parcel) {
        id = parcel.readInt();
        name = parcel.readString();
    }

    public Cuisine(JSONObject json) throws JSONException {
        id = json.getInt("id");
        name = json.getString("name");
    }

    public Cuisine() {
    }

    public Cuisine(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Cuisine createFromParcel(Parcel in) {
            return new Cuisine(in);
        }

        public Cuisine[] newArray(int size) {
            return new Cuisine[size];
        }
    };
}
