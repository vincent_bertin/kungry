package ca.ulaval.ima.mp.model.restaurant;

import ca.ulaval.ima.mp.model.KungryModel;
import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class GetRestaurantInput implements ApiInput {
    private Double latitude;
    private Double longitude;
    private Integer id;

    public GetRestaurantInput() {}

    public GetRestaurantInput(double latitude, double longitude, int id) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public RequestBody toRequestBody() {
        return null;
    }

    @Override
    public String toQueryString() {
        String queryString = "/" + id.toString() + "?";

        if (latitude != null) {
            queryString += "&latitude=" + latitude;
        }
        if (longitude != null) {
            queryString += "&longitude=" + longitude;
        }

        return queryString;
    }
}
