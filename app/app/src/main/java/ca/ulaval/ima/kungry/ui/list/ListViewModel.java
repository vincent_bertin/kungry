package ca.ulaval.ima.mp.ui.list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import ca.ulaval.ima.mp.api.ApiCallback;
import ca.ulaval.ima.mp.model.KungryViewModel;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;

public class ListViewModel extends KungryViewModel {

    private MutableLiveData<List<RestaurantLight>> restaurantLights;

    public ListViewModel() {
    }

    public MutableLiveData<List<RestaurantLight>> getRestaurantLights() {
        if (restaurantLights == null) {
            restaurantLights = new MutableLiveData<List<RestaurantLight>>();
            loadRestaurantsLight();
        }
        return restaurantLights;
    }

    private void loadRestaurantsLight() {
        final ListViewModel that = this;

        api.getRestaurants(1, 10, new ApiCallback.ListCallback<RestaurantLight>() {
            @Override
            public void onSuccess(List<RestaurantLight> list) {
                that.restaurantLights.postValue(list);
            }
        });
    }

}