package ca.ulaval.ima.mp.api;

import java.util.List;

public class ApiCallback {
    public static class ListCallback<T> {
        public void onSuccess(List<T> list){};
    }
    public static class ItemCallback<T> {
        public void onSuccess(T item){};
    }
    public static class Callback {
        public void done() {}
    }
}
