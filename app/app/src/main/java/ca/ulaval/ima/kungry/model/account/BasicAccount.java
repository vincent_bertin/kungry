package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import ca.ulaval.ima.mp.model.KungryModel;

public class BasicAccount extends KungryModel {

    private String clientId;
    private String clientSecret;
    private String email;

    protected BasicAccount(Parcel parcel) {
        clientId = parcel.readString();
        clientSecret = parcel.readString();
        email = parcel.readString();
    }

    public BasicAccount() {}

    public BasicAccount(String clientId, String clientSecret, String email) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.email = email;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(clientId);
        parcel.writeString(clientSecret);
        parcel.writeString(email);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public BasicAccount createFromParcel(Parcel in) {
            return new BasicAccount(in);
        }

        public BasicAccount[] newArray(int size) {
            return new BasicAccount[size];
        }
    };
}
