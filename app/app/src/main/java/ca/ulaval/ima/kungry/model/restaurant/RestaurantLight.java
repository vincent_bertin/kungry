package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ca.ulaval.ima.mp.model.KungryModel;

public class RestaurantLight extends KungryModel {
    private Integer id;
    private String name;
    private List<Cuisine> cuisine;
    private Integer reviewCount;
    private String reviewAverage;
    private String image;
    private String distance;
    private String location;

    public RestaurantLight(JSONObject json) throws JSONException {
        id = json.getInt("id");
        name = json.getString("name");
        cuisine = new ArrayList<>();
        JSONArray cuisineJson = json.getJSONArray("cuisine");
        for (int i = 0; i < cuisineJson.length(); i++) {
            Cuisine cuisineItem = new Cuisine(cuisineJson.getJSONObject(i));
            cuisine.add(cuisineItem);
        }
        reviewCount = json.getInt("review_count");
        reviewAverage = json.getString("review_average");
        image = json.getString("image");
        distance = json.getString("distance");
        location = json.getString("location");
    }

    public RestaurantLight() {}

    public RestaurantLight(Parcel parcel) {
        id = parcel.readInt();
        name = parcel.readString();
        cuisine = parcel.readParcelable(Cuisine.class.getClassLoader());
        reviewCount = parcel.readInt();
        reviewAverage = parcel.readString();
        image = parcel.readString();
        distance = parcel.readString();
        location = parcel.readString();
    }

    public RestaurantLight(Integer id, String name, List<Cuisine> cuisine, Integer reviewCount,
                           String reviewAverage, String image, String distance, String location) {
        this.id = id;
        this.name = name;
        this.cuisine = cuisine;
        this.reviewCount = reviewCount;
        this.reviewAverage = reviewAverage;
        this.image = image;
        this.distance = distance;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Cuisine> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<Cuisine> cuisine) {
        this.cuisine = cuisine;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewAverage() {
        return reviewAverage;
    }

    public void setReviewAverage(String reviewAverage) {
        this.reviewAverage = reviewAverage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        Parcelable[] cuisineArray = new Parcelable[cuisine.size()];
        cuisine.toArray(cuisineArray);
        parcel.writeTypedArray(cuisineArray, 0);
        parcel.writeInt(reviewCount);
        parcel.writeString(reviewAverage);
        parcel.writeString(image);
        parcel.writeString(distance);
        parcel.writeString(location);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public RestaurantLight createFromParcel(Parcel in) {
            return new RestaurantLight(in);
        }

        public RestaurantLight[] newArray(int size) {
            return new RestaurantLight[size];
        }
    };
}
