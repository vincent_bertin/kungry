package ca.ulaval.ima.mp.ui.list;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import ca.ulaval.ima.mp.R;
import ca.ulaval.ima.mp.model.restaurant.RestaurantLight;

public class ListFragment extends Fragment {

    private ListViewModel listViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(this).get(ListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list, container, false);

        listViewModel.getRestaurantLights().observe(getViewLifecycleOwner(),
                new Observer<List<RestaurantLight>>() {
            @Override
            public void onChanged(List<RestaurantLight> restaurantLights) {
                for (RestaurantLight item : restaurantLights) {
                    Log.d("Kungry", item.getName());
                }
            }
        });

        listViewModel.getRestaurantLights().observe(getViewLifecycleOwner(), new Observer<List<RestaurantLight>>() {
            @Override
            public void onChanged(List<RestaurantLight> restaurantLights) {
                for (RestaurantLight item : restaurantLights) {
                    Log.d("Kungry", item.getName());
                }
            }
        });
        return root;
    }
}