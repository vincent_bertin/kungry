package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import ca.ulaval.ima.mp.model.ApiInput;
import ca.ulaval.ima.mp.model.KungryModel;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateReview extends KungryModel implements ApiInput {
    private Integer restaurantId;
    private Integer stars;
    private String comment;

    private CreateReview(Parcel parcel) {
        restaurantId = parcel.readInt();
        stars = parcel.readInt();
        comment = parcel.readString();
    }

    public CreateReview() {}

    public CreateReview(Integer restaurantId, Integer stars, String comment) {
        this.restaurantId = restaurantId;
        this.stars = stars;
        this.comment = comment;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public RequestBody toRequestBody() {
        return new FormBody.Builder()
                .add("restaurant_id", restaurantId.toString())
                .add("stars", stars.toString())
                .add("comment", comment)
                .build();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(restaurantId);
        parcel.writeInt(stars);
        parcel.writeString(comment);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CreateReview createFromParcel(Parcel in) {
            return new CreateReview(in);
        }

        public CreateReview[] newArray(int size) {
            return new CreateReview[size];
        }
    };

    @Override
    public String toQueryString() {
        return null;
    }
}
