package ca.ulaval.ima.mp.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateAccountCreate extends AccountLogin implements ApiInput {
    private String firstName;
    private String lastName;

    public CreateAccountCreate() {}

    public CreateAccountCreate(Parcel parcel) {
        super(parcel);
        this.firstName = parcel.readString();
        this.lastName = parcel.readString();
    }

    public CreateAccountCreate(String clientId, String clientSecret, String email,
                               String password, String firstName, String lastName) {
        super(clientId, clientSecret, email, password);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CreateAccountCreate createFromParcel(Parcel in) {
            return new CreateAccountCreate(in);
        }

        public CreateAccountCreate[] newArray(int size) {
            return new CreateAccountCreate[size];
        }
    };

    @Override
    public RequestBody toRequestBody() {
        return new FormBody.Builder()
                .add("client_id", getClientId())
                .add("client_secret", getClientSecret())
                .add("first_name", getFirstName())
                .add("last_name", getLastName())
                .add("email", getEmail())
                .add("password", getPassword())
                .build();
    }

    @Override
    public String toQueryString() {
        return null;
    }
}
