package ca.ulaval.ima.mp.model.api;

import ca.ulaval.ima.mp.model.ApiInput;
import okhttp3.RequestBody;

public class PaginatedInput implements ApiInput {
    private Integer page;
    private Integer pageSize;

    public PaginatedInput(Integer page, Integer pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public RequestBody toRequestBody() {
        return null;
    }

    @Override
    public String toQueryString() {
        String queryString = "?";

        if (page != null) {
            queryString += "&page=" + page;
        }
        if (pageSize != null) {
            queryString += "&page_size=" + pageSize;
        }

        return queryString;
    }
}
