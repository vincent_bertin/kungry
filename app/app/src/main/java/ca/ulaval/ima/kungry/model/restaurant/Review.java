package ca.ulaval.ima.mp.model.restaurant;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ca.ulaval.ima.mp.model.KungryModel;

public class Review extends KungryModel {
    private Integer id;
    private ca.ulaval.ima.mp.model.restaurant.Creator creator;
    private Integer stars;
    private String image;
    private String comment;
    private String date;

    private Review(Parcel parcel) {
        id = parcel.readInt();
        creator = parcel.readParcelable(
                ca.ulaval.ima.mp.model.restaurant.Creator.class.getClassLoader());
        stars = parcel.readInt();
        image = parcel.readString();
        comment = parcel.readString();
        date = parcel.readString();
    }

    public Review(JSONObject json) throws JSONException {
        id = json.getInt("id");
        creator = new ca.ulaval.ima.mp.model.restaurant.Creator(json.getJSONObject("creator"));
        stars = json.getInt("stars");
        image = json.getString("image");
        comment = json.getString("comment");
        date = json.getString("date");
    }

    public Review() {}

    public Review(Integer id, ca.ulaval.ima.mp.model.restaurant.Creator creator,
                  Integer stars, String image, String comment, String date) {
        this.id = id;
        this.creator = creator;
        this.stars = stars;
        this.image = image;
        this.comment = comment;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ca.ulaval.ima.mp.model.restaurant.Creator getCreator() {
        return creator;
    }

    public void setCreator(ca.ulaval.ima.mp.model.restaurant.Creator creator) {
        this.creator = creator;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeParcelable(creator, 0);
        parcel.writeInt(stars);
        parcel.writeString(image);
        parcel.writeString(comment);
        parcel.writeString(date);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        public Review[] newArray(int size) {
            return new Review[size];
        }
    };
}
